#include "test.h"
#include "test_utils.h"

#define DEBUG_FILE stdout


// Common successful memory acquisition.
int test_normal() {
    void* heap = heap_init(900);
    void* block = _malloc(512);
    if (block == NULL || heap == NULL) {
        return 1;
    }
    debug_heap(DEBUG_FILE, heap);

    _free(block);
    munmap(heap, size_from_capacity(get_header_from_contents(block)->capacity).bytes);
    return 0;
}

// Freeing one block from several allocated ones.
int test_free_one_block() {
    void* heap = heap_init(900);
    void* block1 = _malloc(100);
    void* block2 = _malloc(100);
    void* block3 = _malloc(100);
    struct block_header* header1 = get_header_from_contents(block1);
    struct block_header* header2 = get_header_from_contents(block2);
    struct block_header* header3 = get_header_from_contents(block3);
    if (heap == NULL || block1 == NULL || block2 == NULL || block3 == NULL) {
        return 1;
    }
    debug_heap(DEBUG_FILE, heap);
    _free(block2);
    if (header1->is_free || !header2->is_free || header3->is_free) {
        return 1;
    }
    debug_heap(DEBUG_FILE, heap);

    _free(block3);
    _free(block1);
    munmap(heap, size_from_capacity(header1->capacity).bytes);
    return 0;
}

// Freeing two blocks from several allocated blocks.
int test_free_two_block() {
    void* heap = heap_init(900);
    void* block1 = _malloc(100);
    void* block2 = _malloc(100);
    void* block3 = _malloc(100);
    void* block4 = _malloc(100);
    void* block5 = _malloc(100);
    struct block_header* header1 = get_header_from_contents(block1);
    struct block_header* header2 = get_header_from_contents(block2);
    struct block_header* header3 = get_header_from_contents(block3);
    struct block_header* header4 = get_header_from_contents(block4);
    struct block_header* header5 = get_header_from_contents(block5);
    if (!heap || !block1 || !block2 || !block3|| !block4 || !block5) {
        return 1;
    }
    debug_heap(DEBUG_FILE, heap);
    _free(block5);
    _free(block2);
    if (header1->is_free || !header2->is_free || header3->is_free || header4->is_free || !header5->is_free) {
        return 1;
    }
    debug_heap(DEBUG_FILE, heap);

    _free(block4);
    _free(block3);
    _free(block1);
    munmap(heap, size_from_capacity(header1->capacity).bytes);
    return 0;
}

// The memory is over, the new memory region expands the old one.
int test_memory_over_common() {
    size_t full_size = capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes;
    void* heap = heap_init(full_size);
    void* block1 = _malloc(full_size);
    struct block_header* header1 = get_header_from_contents(block1);
    debug_heap(DEBUG_FILE, heap);
    if (!heap || !block1 || header1->next || header1->capacity.bytes != full_size) {
        printf("Failed to allocate first block\n");
        return 1;
    }
    void* block2 = _malloc(100);
    struct block_header* header2 = get_header_from_contents(block2);
    if (header1->next != header2 || header2 != (struct block_header*)(header1->contents + full_size)) {
        return 1;
    }
    debug_heap(DEBUG_FILE, heap);

    _free(block2);
    _free(block1);
    munmap(heap, size_from_capacity(header1->capacity).bytes);
    return 0;
}

// The memory is over, the old memory region cannot be expanded due to another allocation
// address range, new region allocated elsewhere
int test_memory_over_selection_in_new_location() {
    size_t full_size = capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes;
    void* heap = heap_init(full_size);
    void* block1 = _malloc(full_size);
    struct block_header* header1 = get_header_from_contents(block1);
    if (!heap || !block1 || header1->next || header1->capacity.bytes != full_size) {
        return 1;
    }
    debug_heap(DEBUG_FILE, heap);
    // Take the next region
    void* res = mmap(
        header1->contents + header1->capacity.bytes,
        REGION_MIN_SIZE,
        PROT_READ | PROT_WRITE,
        MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED,
        -1,
        0
    );
    if (res == MAP_FAILED) {
        printf("Failed to allocate next region\n");
        return 1;
    }
    void* block2 = _malloc(full_size);
    struct block_header* header2 = get_header_from_contents(block2);
    debug_heap(DEBUG_FILE, heap);
    if (header2 == (void*)(header1->contents + header1->capacity.bytes)) {
        return 1;
    }
    return 0;
}
