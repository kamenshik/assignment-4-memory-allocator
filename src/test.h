#ifndef TEST_H
#define TEST_H


#include <stdint.h>
#define __USE_MISC 1
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"


typedef int (*testing_function)();

int test_normal();
int test_free_one_block();
int test_free_two_block();
int test_memory_over_common();
int test_memory_over_selection_in_new_location();

#endif

