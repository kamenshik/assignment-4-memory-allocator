#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#include "mem.h"


extern inline struct block_header* get_header_from_contents(void* contents) {
    return (struct block_header*) ((uint8_t*) contents - offsetof(struct block_header, contents));
}

#endif
