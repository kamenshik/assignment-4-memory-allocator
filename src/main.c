#include <stdint.h>
#include <stdlib.h>

#include "test.h"

#define DEBUG_FILE stdout

struct wrapper_testing_function {
    testing_function test;
    char* name_test;
};

static struct wrapper_testing_function all_testing_functions[] = {
    (struct wrapper_testing_function) {.test=test_normal, .name_test="Normal"},
    (struct wrapper_testing_function) {.test=test_free_one_block, .name_test="Free one block"},
    (struct wrapper_testing_function) {.test=test_free_two_block, .name_test="Free two blocks"},
    (struct wrapper_testing_function) {.test=test_memory_over_common, .name_test="Memory over common"},
    (struct wrapper_testing_function) {
        .test=test_memory_over_selection_in_new_location,
        .name_test="Memory over selection in new location"
    }
};

static int handling_test(struct wrapper_testing_function test, size_t test_num) {
    fprintf(
        DEBUG_FILE,
        "-------------- Test #%" PRIu64 ": %s --------------\n",
        test_num,
        test.name_test
    );
    int result_code = test.test();
    if (result_code != 0) {
        fprintf(
            DEBUG_FILE,
            "Test #%" PRIu64 ": %s - failed\n",
            test_num,
            test.name_test
        );
        return result_code;
    }
    fprintf(
        DEBUG_FILE,
        "----------- Test #%" PRIu64 ": %s passed -----------\n",
        test_num,
        test.name_test
    );
    return 0;
}

static int foreach_all_tests(int (*handling_test)(struct wrapper_testing_function, size_t test_num)) {
    size_t number_of_tests = sizeof(all_testing_functions) / sizeof(all_testing_functions[0]);
    for (size_t i = 0; i < number_of_tests; i++) {
        const int code_return = handling_test(all_testing_functions[i], i + 1);
        if (code_return != 0) {return code_return;}
    }
    return 0;
}

int main() {
    return foreach_all_tests(handling_test);
}
